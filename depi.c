#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

typedef struct {
	int position;
	char *name;
} App;

typedef struct {
	int cursor;
	int nbApp;
	int wApp;
	int hApp;
} Drawer;

App *AppList;

Display *dis;
Drawer d;

int screen;
Window win;
GC gc;

int init_x() {
	unsigned long black,white;

	dis=XOpenDisplay((char *)0);
   	screen=DefaultScreen(dis);
	Screen *s = XScreenOfDisplay(dis, screen);
	black=BlackPixel(dis,screen),	/* get color black */
	white=WhitePixel(dis, screen);  /* get color white */

   	win=XCreateSimpleWindow(dis,DefaultRootWindow(dis),0,0,	
		XWidthOfScreen(s), XHeightOfScreen(s), 0, black, white);

	XSetStandardProperties(dis,win,"My Window","HI!",None,NULL,0,NULL);

	XSelectInput(dis, win, KeyPressMask | KeyReleaseMask);

    gc=XCreateGC(dis, win, 0,0);        

	XSetBackground(dis,gc,white);
	XSetForeground(dis,gc,black);

	XClearWindow(dis, win);
	XMapRaised(dis, win);

	d.cursor = 0;
	d.nbApp = 4;
	d.wApp = (XWidthOfScreen(s)-30)/3;
	d.hApp = XHeightOfScreen(s)/3;
	return screen;
}

void close_x() {
	XFreeGC(dis, gc);
	XDestroyWindow(dis,win);
	XCloseDisplay(dis);
	exit(1);				
}

void drawApps(int first) {
	XDrawRectangle(dis, win, gc, 5, d.hApp, d.wApp, d.hApp);
	XFillRectangle(dis, win, gc, 5, d.hApp, d.wApp, d.hApp);
	XDrawRectangle(dis, win, gc, 15+d.wApp, d.hApp, d.wApp, d.hApp);
	XFillRectangle(dis, win, gc, 30+d.wApp, d.hApp, d.wApp, d.hApp);
	XDrawRectangle(dis, win, gc, 25+2*d.wApp, d.hApp, d.wApp, d.hApp);
	XFillRectangle(dis, win, gc, 25+2*d.wApp, d.hApp, d.wApp, d.hApp);
}

void drawCursor(int pos) {
	XDrawRectangle(dis, win, gc, d.wApp/2 + d.wApp*pos, d.hApp-70, 50, 50);
}

void draw() {
	Screen *s = XScreenOfDisplay(dis, screen);
	XClearArea(dis, win, 0, 0, XWidthOfScreen(s), XHeightOfScreen(s), False);
	int leftApp = 0;
	if (d.cursor == 0) {
		// Left
		drawApps(0);
		drawCursor(0);
	} else if (d.cursor == d.nbApp-1) {
		// Right
		leftApp = d.nbApp-3;
		drawApps(0);
		drawCursor(2);
	} else {
		// Middle
		leftApp = d.cursor;
		drawApps(0);
		drawCursor(1);
	}
}

int main(void)
{
	// Initialize apps
	AppList = malloc(4 * sizeof(App));
	AppList[0].name = malloc(sizeof(char*));
	AppList[0].name = "1";
	AppList[0].position = 0;
	AppList[1].name = malloc(sizeof(char*));
	AppList[1].name = "2";
	AppList[1].position = 1;
	AppList[2].name = malloc(sizeof(char*));
	AppList[2].name = "3";
	AppList[2].position = 2;
	AppList[3].name = malloc(sizeof(char*));
	AppList[3].name = "4";
	AppList[3].position = 3;
	// Initialize graphics
	init_x();
	draw();

	XEvent event;
	KeySym key;	
	char text[255];
	XSetInputFocus(dis, win, RevertToNone, CurrentTime);
	/* look for events forever... */
	while(1) {
		XNextEvent(dis, &event);
		if (event.type==KeyPress) {
			if (XLookupString(&event.xkey,text,255,&key,0)==1) {
				if (text[0]=='q') {
					close_x();
				}
				if (text[0]=='a') {
					system("/home/pi/depi/sockerStart.sh");
				}
				printf("You pressed the %c key!\n",text[0]);
			}
			if (XLookupKeysym(&event.xkey, 0) == XK_Left) {
				if (d.cursor > 0) {
					d.cursor--;
					draw();
					continue;
				}
			}
			if (XLookupKeysym(&event.xkey, 0) == XK_Right) {
				if (d.cursor < d.nbApp) {
					d.cursor++;
					draw();
					continue;
				}
			}
		}
	}
}
