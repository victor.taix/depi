PREFIX?=/usr/X11R6
CFLAGS?=-Os -pedantic -Wall

all:
	$(CC) $(CFLAGS) -I$(PREFIX)/include depi.c -L$(PREFIX)/lib -lX11 -o depi

install:
	cp depi /usr/bin/depi

clean:
	rm -f depi